Wildfly
----------
1. Start Wildfly in standalone mode:

        ${AS}/bin/standalone.sh --server-config=standalone-full.xml

2. Create an application user:

        ${AS}/bin/add-user.sh
        realm=ApplicationRealm UserName=ekiriche Password=ekiriche Group=guest

3. Build and deploy the quickstart

        mvn install -Pdeploy,wildfly

4. Try to use some rest on http://localhost:8080/rest-binding/create/user

Content-type is application/x-www-form-urlencoded