package com.dodidone.market.mates.test.task;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * Description here
 *
 * @author Evgeniy Kirichenko
 * @since 15.3
 */
public class EmptyHeadersException extends Exception {

    /**
     * Set of headers supposed to be filled.
     */
    private Set<String> headers;

    /**
     * Initial string to construct main message.
     */
    private static final String INITIAL_ERROR_MESSAGE = "Following headers should be set: ";

    /**
     * Constructor.
     * @param headerNames - the set of headers to be set.
     */
    public EmptyHeadersException(Collection<String> headerNames) {
        this.headers = new HashSet<String>(headerNames);
    }

    /**
     * Create mesage from its necessary headers.
     * @return String with headers to be filled.
     */
    public String constructErrorString() {
        final StringBuilder sb = new StringBuilder(INITIAL_ERROR_MESSAGE);
        final Iterator<String> iterator = headers.iterator();
        while (iterator.hasNext()) {
            sb.append(iterator.next());
            if (iterator.hasNext()) {
                sb.append(", ");
            }
        }
        return sb.toString();
    }
}
