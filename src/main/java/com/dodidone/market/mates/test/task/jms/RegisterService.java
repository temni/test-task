package com.dodidone.market.mates.test.task.jms;

import com.dodidone.market.mates.test.task.InputData;

public interface RegisterService {

    /**
     * Register the user in the DB.
     *
     * @param user the user
     */
    void register(InputData user);
}
