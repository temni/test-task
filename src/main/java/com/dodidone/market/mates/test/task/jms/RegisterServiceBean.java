package com.dodidone.market.mates.test.task.jms;

import com.dodidone.market.mates.test.task.Constants;
import com.dodidone.market.mates.test.task.InputData;
import com.dodidone.market.mates.test.task.jpa.User;
import org.apache.log4j.Logger;
import org.jboss.resteasy.client.ClientRequest;
import org.jboss.resteasy.client.ClientResponse;
import org.switchyard.component.bean.Reference;
import org.switchyard.component.bean.Service;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceUnit    ;

/**
 * The type Register service bean.
 */
@Service(name= "RegisterService", value = RegisterService.class)
public class RegisterServiceBean implements RegisterService {

    private enum Result {
        /**
         * The SUCCESS.
         */
        SUCCESS("OK"),
        /**
         * The FAULT.
         */
        FAULT("fail");

        private String type;

        /**
         * Instantiates a new Result.
         *
         * @param type the type
         */
        Result(String type) {
            this.type = type;
        }

        @Override
        public String toString() {
            return this.type;
        }
    }

    private static final Logger LOGGER = Logger.getLogger(RegisterService.class);

    @PersistenceUnit(unitName="UserPersistenceUnit")
    private EntityManagerFactory entityManagerFactory;

    @Inject
    @Reference
    private RegisterService registerService;

    @Override
    public void register(InputData inputData) {
        LOGGER.info("got it " + inputData);
        final EntityManager em = entityManagerFactory.createEntityManager();
        final EntityTransaction et = em.getTransaction();
        try {
            et.begin();
            final User user = new User();
            user.setLogin(inputData.getLogin());
            user.setPassword(inputData.getPassword());
            em.persist(user);
            et.commit();
        } catch (Exception exception) {
            LOGGER.warn(String.format("User %s already exist in the database", inputData.getLogin()));
            sendResult(inputData.getCallBackUrl(), inputData.getRequestId(), Result.FAULT, inputData);
        } finally {
            em.close();
        }
        sendResult(inputData.getCallBackUrl(), inputData.getRequestId(), Result.SUCCESS, inputData);
    }


    /**
     * Send result.
     *
     * @param url the url
     * @param requestId the request id
     * @param result the result
     */
    private void sendResult(final String url, final String requestId, final Result result, InputData inputData ) {

        boolean isSendSuccessful = false;
        try {
            final ClientRequest request = new ClientRequest(url);
            final String input = "{\"" + Constants.REQUEST_ID_HEADER_NAME + "\":\"" + requestId + "\", " +
                    "\"Result\":\"" + result + "\"}";
            request.body("application/json", input);

            final ClientResponse<String> response = request.post(String.class);

            isSendSuccessful =  (response.getStatus() != 201 && response.getStatus() != 200);
        } catch (Exception e) {
            LOGGER.warn(String.format("Exception occurred while trying to connect to %s", url), e);
        }

        if (!isSendSuccessful) {
            LOGGER.info("Sending it back to the service in 10 seconds...");
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                LOGGER.error("Something unexpectable occurred");
            }
            registerService.register(inputData);
        }
    }
}
