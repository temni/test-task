package com.dodidone.market.mates.test.task;

/**
 * Description here
 *
 * @author Evgeniy Kirichenko
 * @since 15.3
 */
public class Constants {

    /**
     * Utility class does not require the constructor.
     */
    private Constants(){};

    /**
     * Call back url header name.
     */
    public static final String CALLBACK_URL_HEADER_NAME = "CALLBACK_URL";

    /**
     * Request ID header name.
     */
    public static final String REQUEST_ID_HEADER_NAME = "REQUEST_ID";
}
