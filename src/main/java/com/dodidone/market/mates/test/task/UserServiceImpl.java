package com.dodidone.market.mates.test.task;

import com.dodidone.market.mates.test.task.jms.RegisterService;
import com.dodidone.market.mates.test.task.jpa.User;
import org.apache.log4j.Logger;
import org.switchyard.component.bean.Reference;
import org.switchyard.component.bean.Service;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

@Service(UserService.class)
public class UserServiceImpl implements UserService {

    private static final Logger LOGGER = Logger.getLogger(UserService.class);

    @Inject
    @Reference
    private RegisterService registerService;

    @Override
    public String registerUser(final InputData data) throws Exception {
        if (data.getLogin() == null || data.getLogin().isEmpty()) {
            throw new EmptyLoginException();
        }
        final List<String> arrayOfHeaders = new ArrayList<String>();
        if (data.getRequestId() == null || data.getRequestId().isEmpty()) {
            arrayOfHeaders.add(Constants.REQUEST_ID_HEADER_NAME);
        }
        if (data.getCallBackUrl() == null || data.getCallBackUrl().isEmpty()) {
            arrayOfHeaders.add(Constants.CALLBACK_URL_HEADER_NAME);
        }
        if (!arrayOfHeaders.isEmpty()) {
            throw new EmptyHeadersException(arrayOfHeaders);
        }
        new Thread(new Runnable() {
            @Override
            public void run() {
                LOGGER.info("Calling JMS service async");
                registerService.register(data);
            }
        }).start();
        return "SUCCESS";
    }
}
