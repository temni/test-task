package com.dodidone.market.mates.test.task;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class EmptyHeadersExceptionMapper implements ExceptionMapper<EmptyHeadersException> {

    @Context
    private HttpHeaders headers;

    @Override
    public Response toResponse(EmptyHeadersException ex) {
        return Response
                .status(Response.Status.PRECONDITION_FAILED)
                .entity(ex.constructErrorString())
                .type(headers.getMediaType() != null ? headers.getMediaType() : MediaType.TEXT_XML_TYPE)
                .build();
    }
}
