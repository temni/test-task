package com.dodidone.market.mates.test.task;

import org.switchyard.Exchange;
import org.switchyard.ExchangeState;
import org.switchyard.HandlerException;
import org.switchyard.Message;

import org.switchyard.component.resteasy.composer.RESTEasyBindingData;
import org.switchyard.component.resteasy.composer.RESTEasyMessageComposer;

import java.util.List;

public class CustomComposer extends RESTEasyMessageComposer {

    /**
     * {@inheritDoc}
     */
    @Override
    public Message compose(RESTEasyBindingData source, Exchange exchange) throws Exception {
        final Message message = super.compose(source, exchange);
        if (source.getOperationName().equals("registerUser")) {
            final String userName = (String) source.getParameters()[0];
            final String password = (String) source.getParameters()[1];
            final List<String> callBackUrl = source.getHeaders().get(Constants.CALLBACK_URL_HEADER_NAME);
            final List<String> requestId = source.getHeaders().get(Constants.REQUEST_ID_HEADER_NAME);

            // Wrap the parameters
            final InputData inputData = new InputData(
                    userName, password,
                    callBackUrl == null || callBackUrl.isEmpty() ? "" : callBackUrl.get(0),
                    requestId == null || requestId.isEmpty() ? "" : requestId.get(0));
            message.setContent(inputData);
        }
        return message;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public RESTEasyBindingData decompose(Exchange exchange, RESTEasyBindingData target) throws Exception {
        final Object content = exchange.getMessage().getContent();
        target = super.decompose(exchange, target);
        if (exchange.getState().equals(ExchangeState.FAULT)) {
            if (content instanceof HandlerException) {
                HandlerException he = (HandlerException) content;
                if (he.getCause() instanceof EmptyLoginException || he.getCause() instanceof EmptyHeadersException) {
                    throw (Exception) he.getCause();
                }
            }
        }
        return target;
    }
}
