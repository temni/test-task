package com.dodidone.market.mates.test.task;

import javax.ws.rs.FormParam;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;


@Path("/create")
public interface UserResource {

    @POST
    @Path("/user")
    @Produces("text/plain")
    public Response registerUser(
            @FormParam("login") String login,
            @FormParam("password") String password,
            @HeaderParam("CALLBACK_URL") String callBackUrl,
            @HeaderParam("REQUEST_ID") String requestId) throws Exception;

}
