package com.dodidone.market.mates.test.task;

/**
 * Description here
 *
 * @author Evgeniy Kirichenko
 * @since 15.3
 */
public class EmptyLoginException extends Exception {

    /**
     * Common style message for an exception.
     */
    private static final String COMMON_STYLE_MESSAGE = "Login parameter is required";

    /**
     * Simple constructor.
     */
    public EmptyLoginException() {
        super(COMMON_STYLE_MESSAGE);
    }

    /**
     * Advanced constructor.
     * @param message - message to be set.
     */
    public EmptyLoginException(String message) {
        super(message);
    }
}
