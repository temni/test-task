package com.dodidone.market.mates.test.task;

/**
 * Description here
 *
 * @author Evgeniy Kirichenko
 * @since 15.3
 */
public class InputData {
    /**
     * The Login.
     */
    private String login;

    /**
     * The Password.
     */
    private String password;

    /**
     * Call back URL.
     */
    private String callBackUrl;

    /**
     * Request ID.
     */
    private String requestId;

    /**
     * Instantiates a new Input data.
     *
     * @param login the login
     * @param password the password
     */
    public InputData(String login, String password) {
        this.login = login;
        this.password = password;
    }

    /**
     * Instantiates a new Input data.
     */
    public InputData() {
    }

    public InputData(String login, String password, String callBackUrl, String requestId) {
        this.login = login;
        this.password = password;
        this.callBackUrl = callBackUrl;
        this.requestId = requestId;
    }

    /**
     * Gets login.
     *
     * @return the login
     */
    public String getLogin() {
        return login;
    }

    /**
     * Sets login.
     *
     * @param login the login
     */
    public void setLogin(String login) {
        this.login = login;
    }

    /**
     * Gets password.
     *
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * Sets password.
     *
     * @param password the password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Gets call back url.
     *
     * @return the call back url
     */
    public String getCallBackUrl() {
        return callBackUrl;
    }

    /**
     * Sets call back url.
     *
     * @param callBackUrl the call back url
     */
    public void setCallBackUrl(String callBackUrl) {
        this.callBackUrl = callBackUrl;
    }

    /**
     * Gets request id.
     *
     * @return the request id
     */
    public String getRequestId() {
        return requestId;
    }

    /**
     * Sets request id.
     *
     * @param requestId the request id
     */
    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    @Override
    public String toString() {
        return "InputData{" +
                "login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", callBackUrl='" + callBackUrl + '\'' +
                ", requestId='" + requestId + '\'' +
                '}';
    }
}
